package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * @author Savelii Zagurskii
 */
@RealmClass
@Root(name = "image", strict = false)
public class ChannelImage extends RealmObject {
    @PrimaryKey
    @Element(name = "link")
    private String link;

    @Element(name = "title")
    private String title;

    @Element(name = "url")
    private String url;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}