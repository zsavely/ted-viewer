package ru.zsavely.ted.viewer.rest;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.squareup.okhttp.OkHttpClient;

import java.util.UUID;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.SimpleXMLConverter;
import ru.zsavely.ted.viewer.rest.model.ChannelItem;
import ru.zsavely.ted.viewer.rest.model.RootElement;
import ru.zsavely.ted.viewer.ui.interfaces.ResultListener;

/**
 * @author Savelii Zagurskii
 */
public class RestClient implements Callback<RootElement> {
    private static final String BASE_URL = "http://www.ted.com";

    private static RestClient instance;

    private ResultListener listener;
    private Context context;

    private RestAdapter restAdapter;
    private TedService service;

    public static RestClient getInstance(Context context) {
        RestClient localInstance = instance;

        if (localInstance == null) {
            synchronized (RestClient.class) {
                if (localInstance == null) {
                    instance = localInstance = new RestClient();
                }
            }
        }
        localInstance.setContext(context);

        return localInstance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setOnResultListener(ResultListener listener) {
        this.listener = listener;
    }

    public void getVideos() {
        service.getChannel(this);
    }

    @Override
    public void success(RootElement rootElement, Response response) {
        addModelToRealm(rootElement);
    }

    @Override
    public void failure(RetrofitError error) {
        if (listener != null)
            listener.onFailure();
    }

    private RestClient() {
        restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(new OkHttpClient()))
                .setEndpoint(BASE_URL)
                .setConverter(new SimpleXMLConverter())
                .build();
        service = restAdapter.create(TedService.class);
    }

    private void addModelToRealm(final RootElement rootElement) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                initializePrimaryKeys(rootElement);
                Realm realm = Realm.getInstance(context);

                realm.beginTransaction();
                realm.copyToRealmOrUpdate(rootElement);
                realm.commitTransaction();

                realm.close();

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            listener.onDataSetChanged();
                        }
                    }
                });
            }
        }).start();
    }

    private void initializePrimaryKeys(RootElement rootElement) {
        rootElement.setUuid(UUID.randomUUID().toString());
        for (ChannelItem item : rootElement.getChannel().getItems()) {
            item.getImageUrl().setUuid(UUID.randomUUID().toString());
            item.getMediaGroup().setUuid(UUID.randomUUID().toString());
        }
    }
}