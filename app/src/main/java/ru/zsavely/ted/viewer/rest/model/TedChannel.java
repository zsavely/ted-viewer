package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * @author Savelii Zagurskii
 */
@RealmClass
@Root(name = "channel", strict = false)
public class TedChannel extends RealmObject {
    @PrimaryKey
    @Element(name = "link")
    private String link;

    @Element(name = "title")
    private String title;

    @Element(name = "description")
    private String description;

    @Element(name = "image", type = ChannelImage.class)
    private ChannelImage image;

    @Element(name = "lastBuildDate")
    private String lastBuildDate;

    @ElementList(name = "item", inline = true)
    private RealmList<ChannelItem> items;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ChannelImage getImage() {
        return image;
    }

    public void setImage(ChannelImage image) {
        this.image = image;
    }

    public String getLastBuildDate() {
        return lastBuildDate;
    }

    public void setLastBuildDate(String lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }

    public RealmList<ChannelItem> getItems() {
        return items;
    }

    public void setItems(RealmList<ChannelItem> items) {
        this.items = items;
    }
}