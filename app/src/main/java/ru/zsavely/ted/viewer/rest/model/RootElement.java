package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by SavelyNote on 05.07.2015.
 */
@RealmClass
@Root(name = "rss", strict = false)
public class RootElement extends RealmObject {
    @PrimaryKey
    private String uuid;

    @Element(name = "channel", type = TedChannel.class)
    private TedChannel channel;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public TedChannel getChannel() {
        return channel;
    }

    public void setChannel(TedChannel channel) {
        this.channel = channel;
    }
}