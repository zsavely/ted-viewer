package ru.zsavely.ted.viewer.ui;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ru.zsavely.ted.viewer.R;
import ru.zsavely.ted.viewer.rest.model.MediaContent;

/**
 * @author Savelii Zagurskii
 */
public class AlertListAdapter extends BaseAdapter {

    List<MediaContent> mData;
    Context mContext;
    LayoutInflater inflater;

    public AlertListAdapter(List<MediaContent> data, Context context) {
        mData = data;
        mContext = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_dialog, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.textview_alertdialog);
        textView.setText(String.valueOf(mData.get(position).getBitrate() + " kbs"));

        return convertView;
    }
}
