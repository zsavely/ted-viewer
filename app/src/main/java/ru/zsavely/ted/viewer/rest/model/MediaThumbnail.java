package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * @author Savelii Zagurskii
 */
@RealmClass
@Root(name = "thumbnail", strict = false)
@Namespace(reference = "http://search.yahoo.com/mrss/", prefix = "media")
public class MediaThumbnail extends RealmObject {

    @PrimaryKey
    @Attribute(name = "url")
    private String url;

    @Attribute(name = "width")
    private long width;

    @Attribute(name = "height")
    private long height;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }
}