package ru.zsavely.ted.viewer.rest;

import retrofit.Callback;
import retrofit.http.GET;
import ru.zsavely.ted.viewer.rest.model.RootElement;

/**
 * @author Savelii Zagurskii
 */
public interface TedService {
    @GET("/themes/rss/id/6")
    void getChannel(Callback<RootElement> callback);
}