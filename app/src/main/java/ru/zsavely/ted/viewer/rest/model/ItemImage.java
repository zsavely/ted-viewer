package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * @author Savelii Zagurskii
 */
@RealmClass
@Root(name = "image", strict = false)
@Namespace(reference = "http://www.itunes.com/dtds/podcast-1.0.dtd", prefix = "itunes")
public class ItemImage extends RealmObject {
    // TODO INSERT UUID!!!
    @PrimaryKey
    private String uuid;

    @Attribute(name = "url")
    private String url;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}