package ru.zsavely.ted.viewer.ui.imagehelper;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import ru.zsavely.ted.viewer.R;

/**
 * @author Savelii Zagurskii
 */
public class ImageLoaderHelper {

    private static DisplayImageOptions mOptions;
    private static DisplayImageOptions mOptionsRounded;
    private static DisplayImageOptions.Builder builder;

    public static DisplayImageOptions getDisplayImageOptions() {
        if (mOptions == null) {
            mOptions = getDefault()
                    .showImageOnLoading(R.mipmap.ic_loading)
                    .showImageOnFail(R.mipmap.ic_error).build();
        }
        return mOptions;
    }

    private static DisplayImageOptions.Builder getDefault() {
        if (builder == null) {
            builder = new DisplayImageOptions.Builder()
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .resetViewBeforeLoading(true)
                    .cacheInMemory(true).cacheOnDisk(true);
        }
        return builder;
    }
}