package ru.zsavely.ted.viewer.ui;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import ru.zsavely.ted.viewer.R;

/**
 * @author Savelii Zagurskii
 */
public class ActivityVideo extends AppCompatActivity {
    public static final String URL = "VIDEO_URL";

    private String url;

    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        if (getIntent() != null) {
            url = getIntent().getStringExtra(URL);
        }

        mVideoView = (VideoView) findViewById(R.id.videoview);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(mVideoView);
        Uri video = Uri.parse(url);
        mVideoView.setMediaController(mediaController);
        mVideoView.setVideoURI(video);
        mVideoView.start();
    }
}