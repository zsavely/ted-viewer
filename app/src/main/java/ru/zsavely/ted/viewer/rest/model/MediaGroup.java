package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * @author Savelii Zagurskii
 */
@RealmClass
@Root(name = "group", strict = false)
@Namespace(reference = "http://search.yahoo.com/mrss/", prefix = "media")
public class MediaGroup extends RealmObject {

    /**
     * TODO Insert uuid when downloaded!
     */
    @PrimaryKey
    private String uuid;

    @ElementList(name = "content", inline = true)
    @Namespace(prefix = "media")
    private RealmList<MediaContent> contents;

    @ElementList(name = "thumbnail", inline = true)
    @Namespace(prefix = "media")
    private RealmList<MediaThumbnail> thumbnails;

    @ElementList(name = "credit", inline = true)
    @Namespace(prefix = "media")
    private RealmList<MediaCredit> credits;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public RealmList<MediaContent> getContents() {
        return contents;
    }

    public void setContents(RealmList<MediaContent> contents) {
        this.contents = contents;
    }

    public RealmList<MediaThumbnail> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(RealmList<MediaThumbnail> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public RealmList<MediaCredit> getCredits() {
        return credits;
    }

    public void setCredits(RealmList<MediaCredit> credits) {
        this.credits = credits;
    }
}