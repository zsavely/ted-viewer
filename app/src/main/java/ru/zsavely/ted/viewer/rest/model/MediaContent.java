package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * @author Savelii Zagurskii
 */
@RealmClass
@Root(name = "content", strict = false)
@Namespace(reference = "http://search.yahoo.com/mrss/", prefix = "media")
public class MediaContent extends RealmObject {

    @PrimaryKey
    @Attribute(name = "url")
    private String url;

    @Attribute(name = "duration")
    private long duration;

    @Attribute(name = "fileSize")
    private long fileSize;

    @Attribute(name = "type")
    private String type;

    @Attribute(name = "bitrate")
    private long bitrate;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getBitrate() {
        return bitrate;
    }

    public void setBitrate(long bitrate) {
        this.bitrate = bitrate;
    }
}