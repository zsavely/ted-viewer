package ru.zsavely.ted.viewer.ui.interfaces;

/**
 * @author Savelii Zagurskii
 */
public interface ResultListener {
    void onFailure();

    void onDataSetChanged();
}
