package ru.zsavely.ted.viewer.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import io.realm.RealmBaseAdapter;
import io.realm.RealmList;
import ru.zsavely.ted.viewer.R;
import ru.zsavely.ted.viewer.rest.Connectivity;
import ru.zsavely.ted.viewer.rest.model.ChannelItem;
import ru.zsavely.ted.viewer.rest.model.MediaContent;
import ru.zsavely.ted.viewer.ui.ActivityVideo;
import ru.zsavely.ted.viewer.ui.AlertListAdapter;
import ru.zsavely.ted.viewer.ui.base.RealmRecyclerViewAdapter;
import ru.zsavely.ted.viewer.ui.imagehelper.ImageLoaderHelper;

/**
 * @author Savelii Zagurskii
 */
public class TedChannelVideoAdapter extends RealmRecyclerViewAdapter<ChannelItem> {
    private ImageLoader imageLoader;
    private Context context;
    private RecyclerView recyclerView;

    public TedChannelVideoAdapter(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
        initImageLoader();
        setOnScrollListener();
    }

    private void initImageLoader() {
        if (imageLoader != null)
            imageLoader.destroy();
        imageLoader = ImageLoader.getInstance();

        if (!imageLoader.isInited()) {
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    context).defaultDisplayImageOptions(
                    ImageLoaderHelper.getDisplayImageOptions()).build();
            imageLoader.init(config);
        }
    }

    private void setOnScrollListener() {
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    imageLoader.resume();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                imageLoader.pause();
            }
        });
    }

    private class TedChannelVideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View rootView;

        public TextView title;
        public TextView description;
        public ImageView imageView;
        public CardView cardView;

        private RealmBaseAdapter<ChannelItem> realmBaseAdapter;

        public TedChannelVideoViewHolder(View view, RealmBaseAdapter<ChannelItem> realmBaseAdapter) {
            super(view);

            this.realmBaseAdapter = realmBaseAdapter;
            this.rootView = view;

            cardView = (CardView) view.findViewById(R.id.cardview);
            title = (TextView) view.findViewById(R.id.textview_title);
            description = (TextView) view.findViewById(R.id.textview_description);
            imageView = (ImageView) view.findViewById(R.id.imageview_preview);

            cardView.setClickable(true);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i("ClickTesting", String.valueOf(getPosition()));

            ChannelItem item = realmBaseAdapter.getItem(getPosition());

            RealmList<MediaContent> contents = item.getMediaGroup().getContents();
            showDialog(contents.subList(0, contents.size() - 1));
        }

        private void showDialog(final List<MediaContent> bitrates) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(rootView.getContext());
            builder.setTitle(R.string.alertdialog_title);

            final View alertDialog = LayoutInflater.from(rootView.getContext()).inflate(R.layout.alert_dialog, null);
            builder.setView(alertDialog);
            ListView listView = (ListView) alertDialog.findViewById(R.id.listview_bitrates);
            final AlertListAdapter alertListAdapter = new AlertListAdapter(bitrates, rootView.getContext());
            listView.setAdapter(alertListAdapter);
            final AlertDialog dialog = builder.show();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (Connectivity.isConnected(parent.getContext())) {
                        Intent intent = new Intent(rootView.getContext(), ActivityVideo.class);
                        intent.putExtra(ActivityVideo.URL, bitrates.get(position).getUrl());
                        rootView.getContext().startActivity(intent);

                    } else {
                        Toast.makeText(view.getContext(), R.string.error_not_connected, Toast.LENGTH_SHORT).show();
                    }
                    Log.i("OnItemClickDialog", String.valueOf(bitrates.get(position).getBitrate()));
                    dialog.dismiss();
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_video_preview, parent, false);

        CardView cardView = (CardView) v.findViewById(R.id.cardview);
        cardView.setPreventCornerOverlap(true);

        return new TedChannelVideoViewHolder(v, getRealmAdapter());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        TedChannelVideoViewHolder tvh = (TedChannelVideoViewHolder) viewHolder;
        ChannelItem event = getItem(i);
        tvh.title.setText(event.getTitle());
        tvh.description.setText(event.getDescription());
        imageLoader.displayImage(event.getImageUrl().getUrl(), tvh.imageView);

        Log.i("ImageUrl", event.getImageUrl().getUrl());
    }

    /**
     * The inner RealmBaseAdapter
     * recyclerView count is applied here.
     * <p/>
     * {@link #getRealmAdapter()} is defined in {@link RealmRecyclerViewAdapter}.
     */
    @Override
    public int getItemCount() {
        if (getRealmAdapter() != null) {
            return getRealmAdapter().getCount();
        }
        return 0;
    }
}