package ru.zsavely.ted.viewer.rest.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * @author Savelii Zagurskii
 */
@RealmClass
@Root(name = "item", strict = false)
@NamespaceList({
        @Namespace(reference = "http://www.itunes.com/dtds/podcast-1.0.dtd", prefix = "itunes"),
        @Namespace(reference = "http://search.yahoo.com/mrss/", prefix = "media")
})
public class ChannelItem extends RealmObject {
    @PrimaryKey
    @Element(name = "link")
    private String link;

    @Element(name = "title")
    private String title;

    @Element(name = "pubDate")
    private String pubDate;

    @Element(name = "description")
    private String description;

    @Element(name = "image", type = ItemImage.class)
    @Namespace(prefix = "itunes")
    private ItemImage imageUrl;

    @Element(name = "enclosure", type = Enclosure.class)
    private Enclosure enclosure;

    @Element(name = "group", type = MediaGroup.class)
    @Namespace(prefix = "media")
    private MediaGroup mediaGroup;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemImage getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(ItemImage imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public MediaGroup getMediaGroup() {
        return mediaGroup;
    }

    public void setMediaGroup(MediaGroup mediaGroup) {
        this.mediaGroup = mediaGroup;
    }
}