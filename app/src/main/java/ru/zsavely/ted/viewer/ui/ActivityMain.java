package ru.zsavely.ted.viewer.ui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.zsavely.ted.viewer.R;
import ru.zsavely.ted.viewer.rest.RestClient;
import ru.zsavely.ted.viewer.rest.model.ChannelItem;
import ru.zsavely.ted.viewer.ui.adapters.RealmTedVideoAdapter;
import ru.zsavely.ted.viewer.ui.adapters.TedChannelVideoAdapter;
import ru.zsavely.ted.viewer.ui.interfaces.ResultListener;

/**
 * @author Savelii Zagurskii
 */
public class ActivityMain extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ResultListener {
    public static final String AMOUNT_OF_VERTICAL_SCROLL = "AMOUNT_OF_VERTICAL_SCROLL";

    private Realm realm;
    private RestClient restClient;

    private RecyclerView mRecyclerView;

    private SwipeRefreshLayout swipeRefreshLayout;

    private TedChannelVideoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeToolbar();
        initializeRealm();
        initializeSwipeRefreshLayout();
        initializeRestClient();
        initializeRecyclerView();

        update();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null)
            realm.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            update();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFailure() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDataSetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void initializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
    }

    private void initializeRealm() {
        realm = Realm.getInstance(this);
    }

    private void initializeSwipeRefreshLayout() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.color_primary);
    }

    private void initializeRestClient() {
        restClient = RestClient.getInstance(this);
        restClient.setOnResultListener(this);
    }

    private void initializeRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mAdapter = new TedChannelVideoAdapter(this, mRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, getResources().getInteger(R.integer.columns)));
        // staggeredGridLayoutManager = new StaggeredGridLayoutManager(getResources().getInteger(R.integer.columns),
        // StaggeredGridLayoutManager.VERTICAL);
        // mRecyclerView.setLayoutManager(staggeredGridLayoutManager);

        mRecyclerView.setAdapter(mAdapter);
    }

    private void bindData() {
        RealmResults<ChannelItem> channelItems = realm.where(ChannelItem.class).findAll();
        RealmTedVideoAdapter realmTedVideoAdapter = new RealmTedVideoAdapter(this, channelItems, true);

        mAdapter.setRealmAdapter(realmTedVideoAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void update() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }
        if (restClient != null) {
            restClient.getVideos();
        }
    }

    @Override
    public void onRefresh() {
        update();
    }
}