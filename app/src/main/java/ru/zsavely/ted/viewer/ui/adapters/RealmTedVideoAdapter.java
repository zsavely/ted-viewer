package ru.zsavely.ted.viewer.ui.adapters;

import android.content.Context;

import io.realm.RealmResults;
import ru.zsavely.ted.viewer.rest.model.ChannelItem;
import ru.zsavely.ted.viewer.ui.base.RealmModelAdapter;

/**
 * @author Savelii Zagurskii
 */
public class RealmTedVideoAdapter extends RealmModelAdapter<ChannelItem> {

    public RealmTedVideoAdapter(Context context, RealmResults<ChannelItem> realmResults, boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
    }
}